window.onload = initEvents;

var video, btnPlay, playPosition, volumePosition;

function initEvents()
{
	video = document.getElementById('trailer');
	playButton = document.getElementById('playbtn');
	seekBar = document.getElementById('playPosition');
	volumeBar = document.getElementById('volumePosition'); 
	
	playButton.addEventListener('click', onPlayClick);	
	
	video.addEventListener('timeupdate', onVideoTimeUpdate);
	video.addEventListener('click', onPlayClick);
	
	seekBar.addEventListener('change', onPlayPositionChange);
	seekBar.addEventListener('mousedown', onPlayPositionMouseDown);
	volumeBar.addEventListener('change', onVolumeChange);
}


function playVideo()
{
	video.play();
	playButton.src = 'images/pause.png';
}


function pauseVideo()
{
	video.pause();
	playButton.src = 'images/play.png';
}

function onPlayClick()
{
	if (video.paused) 
		playVideo();
	else
		pauseVideo();	
}


function onVideoTimeUpdate()
{
	seekBar.value = (video.currentTime / video.duration) * 100;	
}


function onVolumeChange()
{
	video.volume = volumeBar.value;	
}


function onPlayPositionChange()
{
	video.currentTime = (seekBar.value / 100) * video.duration;	
}


function onPlayPositionMouseDown()
{
	pauseVideo();	
}