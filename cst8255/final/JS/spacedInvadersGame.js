function SpacedInvadersGame (canvas) 
{
	var _ctx = canvas.getContext("2d");
	var _tank = new Tank(20, 460);
	var _invaders = new InvaderGrid(10, 10);
	var _projectiles = [];
	var _keys = new Array(256);
	var _this = this;
	
	this.score = 0;
	this.scoreText = "Score: " + this.score;
	this.lifeCtr = "Lives: " + _tank.lives;
	this.wave = 0;
	this.waveText = "Wave " + this.wave;
	this.gameOver = false;
	this.invadersLose = false;
	

	this.update = function () 
	{
		var elapsedTime = 1/60;
		_this.processInput(elapsedTime);
		_this.updateGameObjects(elapsedTime);
		_this.draw();
	}


	this.handleKeyDown = function (e) 
	{
		console.log(e.keyCode); 
		_keys[e.keyCode] = true;
	}


	this.handleKeyUp = function (e) 
	{ 
		console.log(e.keyCode); 
		_keys[e.keyCode] = false;
	}
	

	this.processInput = function (elapsedTime) 
	{
		if(_keys["A".charCodeAt(0)]) 
		{
			_tank.moveLeft();
		}
		
		if(_keys["D".charCodeAt(0)])
		{
			_tank.moveRight();
		}
		
		if(_keys[32]) 
		{
			var proj = _tank.fire();
			
			if(proj != null) 
			{
				_projectiles.push(proj);
			}
		}
		
		if(_keys[13] && _tank.lives == 0) 
		{
			_this.newGame();
		}
	}
	

	this.newGame = function () 
	{
		_tank.lives = 3;
		_invaders = new InvaderGrid (10, 10);
		_projectiles.splice(0, (_projectiles.length));

		this.gameOver = false;
		this.invadersLose = false;
		this.lifeCtr = "Lives: " + _tank.lives;
		this.score = 0;
		this.scoreText = "Scores: " + this.score;
		this.wave = 0;
		this.waveText = "Wave " + wave;
	}
	

	this.collisionDetect = function (i, proj) 
	{
		if(i.friendlyValue == proj.friendlyValue) 
		{
			return false;
		}
		
		var left = i.x;
		var right = i.x + i.w;
		var top = i.y;
		var bottom = i.y + i.h;
			
		if (proj.x > right || proj.x < left || proj.y > bottom || proj.y < top) 
		{
			return false;
		}
		
		else 
		{
			return true;
		}
	}
	

	this.updateGameObjects = function (elapsedTime) 
	{
		if(this.gameOver) 
		{
			return;
		}
		
		if (!_tank.lives > 0) 
		{
			this.gameOver = true;
		}
		
		this.invadersLose = true;
		
		for (var i in _invaders.invaderGrid) 
		{
			if(_invaders.invaderGrid[i].life) 
			{
				this.invadersLose = false;
			}
		}
		
		if(this.invadersLose) 
		{
			this.wave += 1;
			this.waveText = "Wave " + this.wave;
			
			if(_invaders.invaderGrid.length == 0) 
			{
				_invaders.populate();
			}
			
			for(var i in _invaders.invaderGrid) 
			{
				_invaders.invaderGrid[i].life = true;
			}
			
			_invaders.x = 10;
			_invaders.y = 10;
			_invaders.speed += 0.5;
			_invaders.fireRate -= 0.2;
			_projectiles.splice(0, (_projectiles.length));
			this.invadersLose = false;
		}
		
		_tank.update(elapsedTime);
		_invaders.update();
		
		for(var i in _invaders.invaderGrid) 
		{
			_invaders.invaderGrid[i].update(elapsedTime);
			
			if(_invaders.invaderGrid[i].y >= canvas.height - 30 && _invaders.invaderGrid[i].life) 
			{
				this.gameOver = true;
			}
			
			if(_this.collisionDetect (_invaders.invaderGrid[i], _tank) && _invaders.invaderGrid[i].life) 
			{
				this.gameOver = true;
			}
			
			var randomNum = Math.floor(Math.random() * 100000) + 1;
			
			if (randomNum > 99900) 
			{
				var proj = _invaders.invaderGrid[i].fire();
			
				if(proj != null) 
				{
					_projectiles.push(proj);
				}
			}
			
			for (var proj in _projectiles) 
			{
				if(_invaders.invaderGrid[i].life) 
				{
					if (_this.collisionDetect(_invaders.invaderGrid[i], _projectiles[proj])) 
					{			
						_projectiles.splice(proj, 1);
						_invaders.invaderGrid[i].life = false;
						this.score += 1000;
						this.scoreText = "Score: " + this.score;
						i--;
						proj --;
					}
				}
			}
		}
		
		for(var i in _projectiles) 
		{
			_projectiles[i].update(elapsedTime);
			
			if(_projectiles[i].y < 0 || _projectiles[i].y > canvas.height) 
			{
				_projectiles.splice(i, 1);
				i--;
			}
			
			if(_this.collisionDetect(_tank, _projectiles[i])) 
			{
				_projectiles.splice(i, 1);
				_tank.lives -= 1;
				this.lifeCtr = "Lives: " + _tank.lives;
				this.score -= 1000;
				this.scoreText = "Score: " + this.score;
				
				if(_tank.lives >= 1) 
				{
					_tank.regenerate();
				}
			}
		}
	}
	
	
	this.draw = function () 
	{
		_ctx.clearRect(0, 0, canvas.width, canvas.height);
		
		if(!this.gameOver) 
		{
			for(var i in _invaders.invaderGrid) 
			{
				_invaders.invaderGrid[i].draw(_ctx);
			}
		
			for(var i in _projectiles) 
			{
				_projectiles[i].draw(_ctx);
			}
		
			_tank.draw(_ctx);
			_ctx.fillStyle = "white";
			_ctx.beginPath();
			_ctx.moveTo(20, 480);
			_ctx.lineTo(430, 480);
			_ctx.lineTo(430, 485);
			_ctx.lineTo(20, 485);
			_ctx.lineTo(20, 480);
			_ctx.font = "bold 16px Arial";
			_ctx.fillText(this.scoreText, 10, 20);
			_ctx.fillText(this.lifeCtr, 10, 40);
			_ctx.fillText(this.waveText, 385, 20);
			_ctx.fill();
			
		}
		
		else 
		{
			_ctx.fillText(this.scoreText, 10, 20);
			_ctx.fillText(this.lifeCtr, 10, 40);
			_ctx.fillText(this.waveText, 385, 20);
			_ctx.fillText("Game Over!", canvas.width / 3 + 20, canvas.height / 2);
			_ctx.fillText("Play Again?", canvas.width / 3 + 24, canvas.height / 2 + 16);
			_ctx.fillText("(ENTER)", canvas.width / 3 + 34, canvas.height / 2 + 32);
			_ctx.fill();
		}
	}
}