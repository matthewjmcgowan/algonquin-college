function Stone (x, y) 
{
	var width = 10;
	var height = 10;
	
	this.friendlyValue = 3;
	this.x = x;
	this.y = y;
	this.w = width;
	this.h = height;
	this.live = true;
	
	
	this.draw = function (ctx) 
	{
		if(!this.live) 
		{
			return;
		}
		
		ctx.save();
		ctx.translate(this.x, this.y);
		ctx.fillStyle = "grey";
		ctx.beginPath();
		ctx.lineTo(0, height);
		ctx.lineTo(width, height);
		ctx.lineTo(width, 0);
		ctx.lineTo(0, 0);
		ctx.fill();		
		ctx.restore();
	}
}
