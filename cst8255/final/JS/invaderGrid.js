
function InvaderGrid (x, y) 
{
	var cols = 8;
	var rows = 5;
	var margin = 10;
	var width = 30;
	var height = 20;
	var _this = this;

	this.friendlyValue = 2;
	this.x = x;
	this.y = y;
	this.direction = 1;
	this.invaderGrid = [];
	this.speed = 0.1;
	
	
	this.update = function () 
	{		
		this.x += this.speed * this.direction;
		var right = this.x + (cols * width) + (margin * (cols - 1));
		
		if (this.x < 10) 
		{
			this.direction = 1;
			this.y += 30;
		}
		
		if (right > 440) 
		{
			this.direction = -1;
			this.y += 30;
		}
		
		for (var i in this.invaderGrid) 
		{
			this.invaderGrid[i].x = this.x + ((width + margin) * (i % cols));
			this.invaderGrid[i].y = this.y + ((height + margin) * Math.floor(i / cols));
		}
	}
	
	
	this.populate = function () 
	{
		for(var x = 0; x < cols; x++) 
		{
			for(var y = 0; y < rows; y++) 
			{
				var invaderX = this.x + x * (width + margin);
				var invaderY = this.y + y * (height + margin);
				var invader = new Invader (invaderX, invaderY);
				this.invaderGrid.push(invader);
			}
		}
	}


	this.draw = function(ctx)
	{
		for (var i in this.invaderGrid)
		{
			this.invaderGrid[i].draw(ctx);
		}
	}
}
