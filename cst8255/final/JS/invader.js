
function Invader (x, y) 
{
	var width = 30;
	var height = 20;
	var changeRate = 1;
	var fireRate = 0;
	var invader = new Image();
	invader.src = "images/invader.png";
	var imageChangeRate = changeRate;
	var imageCheck = 0;
	var _this = this;
	
	this.friendlyValue = 2;
	this.fireRate = 0.5;
	this.x = x;
	this.y = y;
	this.w = width;
	this.h = height;
	this.life = true;
	
	
	this.update = function (elapsedTime) 
	{
		imageChangeRate -= elapsedTime;
		_this.imageChange();
		fireRate -= elapsedTime;
	}


	this.imageChange = function () 
	{	
		if(imageChangeRate <= 0) 
		{
			imageChangeRate = changeRate;
			
			if (imageCheck == 1) 
			{
				invader.src = "images/invader.png";
				imageCheck = 0;
			}
			
			else 
			{
				invader.src = "images/invader2.png";
				imageCheck = 1;
			}
		}
	}
	
	
	this.fire = function () 
	{
		if(!this.life) 
		{
			return;
		}
		
		if(fireRate <= 0 ) 
		{
			fireRate = this.fireRate;
			var projectile = new Projectile (this.x, this.y, this);
			return projectile;
		}
	}
	
	
	this.draw = function (ctx) 
	{
		if (!this.life) 
		{
			return;
		}
		
		ctx.save();
		ctx.drawImage(invader, this.x, this.y);
		ctx.restore();
	}
}