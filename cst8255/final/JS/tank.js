
function Tank (x, y) 
{
	var width = 30;
	var height = 20;
	var fireRate = 0;
	var rapidFire = 0.25;
	var tank = new Image();
	tank.src = "images/ship.png";
	
	this.friendlyValue = 1;
	this.x = x;
	this.y = y;
	this.h = height;
	this.w = width;
	this.lives = 3;
	this.movement = 0;
	

	this.update = function (elapsedTime) 
	{
		if(this.x < x) 
		{
			this.x = x;
		}
		
		if (this.x > 400) 
		{
			this.x = 400;
		}
		
		if(this.x >= x && this.x <= 400) 
		{
			this.x += this.movement;
		}
		
		this.movement = 0;
		fireRate -= elapsedTime;
	}
	

	this.moveLeft = function () 
	{
		this.movement -= 3;
	}
	

	this.moveRight = function () 
	{
		this.movement += 3;
	}
	

	this.fire = function () 
	{
		if(fireRate <= 0 ) 
		{
			fireRate = rapidFire;
			var projectile = new Projectile (this.x, this.y, this);
			return projectile;
		}
	}
	

	this.regenerate = function () 
	{
		this.x = x;
		this.y = y;
	}
	
	
	this.draw = function (ctx) 
	{
		ctx.save();
		ctx.drawImage(tank, this.x, this.y);
		ctx.restore();
	}
}

