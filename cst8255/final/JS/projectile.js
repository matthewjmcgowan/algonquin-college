
function Projectile (x, y, owner) 
{
	var speed = 3;

	this.x = x + 15;
	this.y = y;
	this.friendlyValue = owner.friendlyValue;
	this.direction = (this.friendlyValue == 1? -1 : 1);
	

	this.update = function (elapsedTime) 
	{
		this.y += speed * this.direction;
	}
	
	
	this.draw = function (ctx) 
	{
		ctx.save();
		ctx.fillStyle = "yellow";
		ctx.translate(this.x, this.y);
		ctx.beginPath();
		ctx.arc(0, 0, 5, 0, (Math.PI * 2));
		ctx.fill();
		ctx.restore();
	}
}
